# README #

Это РидМи данного репозитория (слово-то какое!)

### Главное на повестке дня ###

Смена времени фазы через "Настройки->Время работы" не работает, просто потому что я не уверена, как это нужно реализовать (нужно показать это окошко до старта работы или обнулять счетчик каждый раз при смене времени через настройки?), и поэтому для этого пока существуют ништяки типа +- минуты щелчком на таймеры в статус-баре.

### Шта эта? ###

* **Краткое описание**
Текстовый редактор, предназначенный для улучшения продуктивности и выработки у пользователя привычки писать, не отвлекаясь на внешние раздражители.

* **Version** 1.1

### Как мне эту чертовщину поставить, бро? ###

* **Сетуп**
Когда-нибудь я настрою инсталлер, который в данный момент лежит [на гугл-диске](https://drive.google.com/file/d/0BxoP6jUkc45lekttZXcta2RJU1E/view?usp=sharing), чтобы он был здесь - и тогда вообще ни о чем задумываться не надо будет х)

* **Проект**
А можно скопировать себе весь проект и запустить экзешник в папке bin/Release. 

### А документация где? ###

Пока что вот тут, в РидМи (и в инстеллере в РидМи, и в папке проекта в РидМи), и минимальная:

**Особенности:** 

* Таймер "смерти" текста, по истечении которого весь набранный в редакторе текст исчезает. Безвозвратно.
* Таймер "смерти" текста по умолчанию 5 секунд. Это значение можно поменять в настройках.
* Текст медленно «исчезает», наглядно демонстрируя сколько времени осталось до срабатывания таймера «смерти».
* Рабочий процесс - "фаза", это время, по истечении которого таймер "смерти" отключается автоматически, позволяя пользователю наконец спокойно вдохнуть и поработать с текстом в обычном режиме.
* "Фаза" длится по умолчанию 5 минут. Это значение можно поменять в настройках.

**Строка статуса** (в нижней части окна приложения) показывает, соответственно, слева-направо следующую информацию:

1.	Количество времени, которое пользователь уже работает в приложении
2.	Текущее количество слов
3.	Количество времени, оставшееся до конца «фазы».
4.	Индикатор работы таймера «смерти» текста.
По нажатию на таймер прошедшего времени (слева) время фазы уменьшается на одну минуту. По нажатию на таймер фазы (справа) – увеличивается на минуту. Нажатие на индикатор количества слов (по середине) останавливает работу таймера «смерти» текста. Повторное нажатие – возобнавляет.



### На кого жаловаться? ###

* На меня: **scanavie@gmail.com**, она же Маша