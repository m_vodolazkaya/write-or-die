﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WriteOrDie
{
    public static class Global
    {
        //public static Timer dieText {get; set;}
        //public static Timer workPhase { get; set; }
        //public static Timer workTime { get; set; }


        public static Timer TIMER;
        public static bool dieTextTimer_IsActive { get; set; }
        public static bool workingPhaseTimer_IsActive { get; set; }
        public static bool workedTimerTimer_IsActive { get; set; }

        public const int defaultWorkPhase = 300;
        public const int defaultDieText = 5;

        public static int workPhaseSeconds { get; set; }
        public static int workedSeconds { get; set; }




    }
}
