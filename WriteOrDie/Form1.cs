﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace WriteOrDie
{
    public partial class Form1 : Form
    {

        #region Singleton
        static readonly Form1 _singletonInstance = new Form1();

        public static Form1 SingletonInstance
        {
            get
            {
                return _singletonInstance;
            }
        }

        #endregion

        // переменная для подсчета и корректного отображения времени
        private DateTime dt;

        // счетчик количества секунд бездействия
        private int dieTextTimer_seconds;


        // переменная для работы "исчезания" текста.
        // должна находиться в интервале от 0 до 255
        int color = 0;


        // DLL libraries used to manage hotkeys
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        const int MYACTION_HOTKEY_ID = 1;

        public Form1()
        {
            InitializeComponent();

            // инициализировать вспомогательную переменную для времени
            dt = new DateTime();

            // инициализировать счетчики времени
            Global.workedSeconds = 0;
            Global.workPhaseSeconds = WriteOrDie.Properties.Settings.Default.WorkPhase;

            // включить все таймеры
            Global.dieTextTimer_IsActive = true;
            Global.workingPhaseTimer_IsActive = true;
            Global.workedTimerTimer_IsActive = true;

            // инициализировать таймер
            Global.TIMER= new Timer();
            Global.TIMER.Interval = 1000; // 1 sec
            Global.TIMER.Tick += new EventHandler(TIMER_Tick);
            Global.TIMER.Start();

            // задать шаблон для строки состояния
            toolStripStatusLabel2.Text = "Количество слов: " + GetWordCount().ToString();
            toolStripStatusLabel1.Text = "Вы уже работаете: " + dt.AddSeconds(Global.workedSeconds).ToString("HH:mm:ss");
            toolStripStatusLabel3.Text = "Осталось работать: " + dt.AddSeconds(Global.workPhaseSeconds).ToString("HH:mm:ss");



            // Modifier keys codes: Alt = 1, Ctrl = 2, Shift = 4, Win = 8
            // Compute the addition of each combination of the keys you want to be pressed
            // ALT+CTRL = 1 + 2 = 3 , CTRL+SHIFT = 2 + 4 = 6...
            RegisterHotKey(this.Handle, MYACTION_HOTKEY_ID, 2, (int)Keys.Space);
        }


        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0312 && m.WParam.ToInt32() == MYACTION_HOTKEY_ID)
            {
                // My hotkey has been typed
                Global.dieTextTimer_IsActive = !Global.dieTextTimer_IsActive;
                Global.workingPhaseTimer_IsActive = !Global.workingPhaseTimer_IsActive;
                richTextBox1.ReadOnly = !richTextBox1.ReadOnly;
            }            
            base.WndProc(ref m);
        }

        /// <summary>
        /// Метод тика таймера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TIMER_Tick(object sender, EventArgs e)
        {
            // таймер проведенного за работой времени
            if (Global.workedTimerTimer_IsActive)
            {
                Global.workedSeconds++;
                toolStripStatusLabel1.Text = "Вы уже работаете: " + dt.AddSeconds(Global.workedSeconds).ToString("HH:mm:ss");
            }

            // таймер обратного отсчета времени фазы
            if (Global.workingPhaseTimer_IsActive)
            {
                Global.workPhaseSeconds--;
                if (Global.workPhaseSeconds <= 0)
                {
                    // выключить таймер смерти текста
                    Global.dieTextTimer_IsActive = false;  
                    
                    // выключить таймер обратного отсчета рабочей фазы
                    Global.workingPhaseTimer_IsActive = false;
                    
                    MessageBox.Show("Поздравляю! Вы завершили рабочую фазу.\rТеперь можно приступить к редактированию текста.", "Ура!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    toolStripStatusLabel3.Text = "Теперь можно работать в обычном режиме";
                }
                else
                {
                    toolStripStatusLabel3.Text = "Осталось работать: " + dt.AddSeconds(Global.workPhaseSeconds).ToString("HH:mm:ss");
                }
            }

            // таймер смерти текста
            if (Global.dieTextTimer_IsActive)
            {
                toolStripStatusLabel4.Image = WriteOrDie.Properties.Resources.TimerIsUp;

                dieTextTimer_seconds++;
                int buf = dieTextTimer_seconds; // необходим, т.к. изменение цвета в rtb вызывает rtb_textChanged, обнуляющий dieTextTimer_seconds

                if (color + (int)(200 / WriteOrDie.Properties.Settings.Default.DieInterval) >= 255)
                {
                    color = 250;
                }
                else
                {
                    color += (int)(200 / WriteOrDie.Properties.Settings.Default.DieInterval);
                }
                int buf2 = color;

                if (dieTextTimer_seconds > WriteOrDie.Properties.Settings.Default.DieInterval)
                {
                    if (richTextBox1.Text != "")
                    {
                        System.Windows.Forms.Clipboard.SetText(richTextBox1.Text);
                    }
                    richTextBox1.Text = "";
                }

                richTextBox1.ForeColor = Color.FromArgb(color, color, color);
                dieTextTimer_seconds = buf;
                color = buf2;
            }
            else
            {
                toolStripStatusLabel4.Image = WriteOrDie.Properties.Resources.TimerIsDown;
                richTextBox1.ForeColor = Color.Black;
            }

        }



        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            // перезапустить таймер смерти текста
            dieTextTimer_seconds = 0;
            color = 0;

            // change word count label
            toolStripStatusLabel2.Text = "Количество слов: " + GetWordCount().ToString();
        }




        /// <summary>
        /// Метод для подсчета поличества слов в richTextBox1. 
        /// Считает слова по делиметрам (небуквенным символам-разделителям). 
        /// </summary>
        /// <returns>Возвращает int количество слов</returns>
        private int GetWordCount()
        {
            // Add space to end of text to ensure the last word is counted.
            MatchCollection wordColl = Regex.Matches(richTextBox1.Text, @"[\W]+");

            if (richTextBox1.Text == "")
                return 0;

            // Количество разделителей на 1 меньше количества слов, потому +1
            return wordColl.Count + 1;
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            // скинуть минуту с рабочей фазы
            Global.workPhaseSeconds -= 60;
        }

        /// <summary>
        /// Меню настроек интервала смерти текста
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void интервалToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DieIntervalSettings DIS = new DieIntervalSettings();
            Global.dieTextTimer_IsActive = false;
            Global.workedTimerTimer_IsActive = false;
            Global.workingPhaseTimer_IsActive = false;
            DIS.ShowDialog();
        }

        private void toolStripStatusLabel3_Click(object sender, EventArgs e)
        {
            // накинуть минуту к рабочей фазе
            Global.workPhaseSeconds += 60;            
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void времяРаботыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkPhaseSettings WPS = new WorkPhaseSettings();
            WPS.Show();
        }

        private void toolStripStatusLabel2_Click(object sender, EventArgs e)
        {
            // поставить/снять с паузы таймер смерти текста
            Global.dieTextTimer_IsActive = !Global.dieTextTimer_IsActive;
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 AB = new AboutBox1();
            AB.Show();
        }
    }
}
