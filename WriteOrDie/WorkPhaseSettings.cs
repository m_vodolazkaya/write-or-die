﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WriteOrDie
{
    public partial class WorkPhaseSettings : Form
    {
        /// <summary>
        /// Конструктор формы
        /// </summary>
        public WorkPhaseSettings()
        {
            InitializeComponent();

            secondsUD.Value = WriteOrDie.Properties.Settings.Default.WorkPhase % 60;
            minutesUD.Value = WriteOrDie.Properties.Settings.Default.WorkPhase / 60;

        }


        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void minutesUD_ValueChanged(object sender, EventArgs e)
        {

        }

        private void WorkPhaseSettings_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            WriteOrDie.Properties.Settings.Default.WorkPhase = (int)(minutesUD.Value) * 60 + (int)secondsUD.Value;
            WriteOrDie.Properties.Settings.Default.Save();
            Global.workPhaseSeconds = Properties.Settings.Default.WorkPhase;
            Global.dieTextTimer_IsActive = true;
            Global.workingPhaseTimer_IsActive = true;

            this.Hide();
            Form1 WorkForm = Form1.SingletonInstance;
            WorkForm.Closed += (s, args) => this.Close();
            WorkForm.Show();
        }
    }
}
