﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WriteOrDie
{
    public partial class DieIntervalSettings : Form
    {
        public DieIntervalSettings()
        {
            InitializeComponent();

            numericUpDown1.Value = WriteOrDie.Properties.Settings.Default.DieInterval;
        }

        Timer timer1;
        Timer timer2;
        Timer timer3;

        public DieIntervalSettings(Timer t1, Timer t2, Timer t3)
        {
            InitializeComponent();

            timer1 = t1;
            timer2 = t2;
            timer3 = t3;

            timer1.Stop();
            timer2.Stop();
            timer3.Stop();

            numericUpDown1.Value = WriteOrDie.Properties.Settings.Default.DieInterval;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > Properties.Settings.Default.DieInterval)
            {
                WriteOrDie.Properties.Settings.Default.DieInterval++;
                WriteOrDie.Properties.Settings.Default.Save();
            }
            else
            {
                WriteOrDie.Properties.Settings.Default.DieInterval--;
                WriteOrDie.Properties.Settings.Default.Save();
            }
        }

        private void DieIntervalSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Start();
            timer2.Start();
            timer3.Start();
        }
    }
}
